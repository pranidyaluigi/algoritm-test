/* 
Expectation
-   `averagePair([-1, 0, 3, 4, 5, 6], 4.1)` => false
-   `averagePair([1, 2, 3], 2.5)` => true
-   `averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8)` => true
-   'averagePair([1, 2, 5], 2)' => false
 */

function averagePair(arr, average) {
  //   console.log(arr[0]);
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if ((arr[i] + arr[j]) / 2 == average) {
        // console.log((arr[i] + arr[j]) / 2);
        return true;
      }
    }
  }
  return false;
}

console.log(averagePair([-1, 0, 3, 4, 5, 6], 4.1));
console.log(averagePair([1, 2, 3], 2.5));
console.log(averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8));
console.log(averagePair([1, 2, 5], 2));
